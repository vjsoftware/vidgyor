<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Video;

class Video extends Model
{
    //Relationships
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
