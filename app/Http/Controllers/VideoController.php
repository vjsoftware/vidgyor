<?php

namespace App\Http\Controllers;
use FFMpeg;

use Auth;

use App\Video;
use Illuminate\Http\Request;
use Carbon\Carbon;

class VideoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Auth::user()->video;
        return view('videos')->with('videos', $videos);
    }


    public function all()
    {
        $videos = Auth::user()->video;
        return $videos;
    }

    public function search(Request $request)
    {
        $search = Video::where('name', 'like', '%'.$request['title'].'%')->get();
        return $data = ['result' => $search];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if($request->hasFile('video')){


        $this->validate($request, [
            'video' => 'required|mimes:mp4,webm,avi,flv|max:500000',
        ]);

          $file = $request->file('video');
          $time = time();
          $videoFileName = $time . '.' . $file->getClientOriginalExtension();

          // Get Duration;
          $ffprobe = FFMpeg\FFProbe::create(
            [
              'ffmpeg.binaries'  => 'C:/FFmpeg/bin/ffmpeg.exe', // the path to the FFMpeg binary
              'ffprobe.binaries' => 'C:/FFmpeg/bin/ffprobe.exe', // the path to the FFProbe binary
            ]
          );

          $duration = $ffprobe
              ->format($file) // extracts file informations
              ->get('duration');

          $durationFormat = gmdate("i:s", round($duration));

          $filename = $file->getClientOriginalName();
          $filenamenoExt = pathinfo($filename, PATHINFO_FILENAME);
          $path = public_path().'/uploads/';
          $filepath = $file->move($path, $videoFileName);

          // Get Thumbnail
          $ffmpeg = FFMpeg\FFMpeg::create(
            [
              'ffmpeg.binaries'  => 'C:/FFmpeg/bin/ffmpeg.exe', // the path to the FFMpeg binary
              'ffprobe.binaries' => 'C:/FFmpeg/bin/ffprobe.exe', // the path to the FFProbe binary
            ]
          );
          $frameName = 'thumbnails/'.$time.'.jpg';
          $video = $ffmpeg->open($filepath);
          $video
              ->filters()
              ->resize(new FFMpeg\Coordinate\Dimension(320, 240))
              ->synchronize();
          $video
              ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(10))
              ->save($frameName);
          $newVideo = new Video();
          $newVideo->user_id = Auth::user()->id;
          $newVideo->name = $filenamenoExt;
          $newVideo->time_name = $videoFileName;
          $newVideo->duration = $durationFormat;
          $newVideo->thumb = $frameName;
          $newVideo->save();
          return redirect('videos');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        // return 'pop';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('edit')->with('video', $video);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
      // return $video;
        $video->name = $request->name;
        $video->desc = $request->desc;
        $video->tags = $request->tags;
        $video->category = $request->category;
        $video->save();
        return redirect('videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
      // return $video;
        // $video = Video::find($video);
        unlink($video['thumb']);
        unlink('uploads/'.$video['time_name']);
        $video->delete();

        return $data = ['status' => 'success'];
    }
}
