/*
 Template Name: Delexa - Material Design Admin & Dashboard Template
 Author: Myra Studio
 File: Datatables
*/
$(document).ready(function() {
    $("#basic-datatable").DataTable({
      "searching": false,
      "paginate": false,
      "bInfo" : false,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });
});
