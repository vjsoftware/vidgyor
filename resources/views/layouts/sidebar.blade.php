<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <a href="{{ route('videos.index') }}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span>Videos</span></a>
                </li>
                <li>
                    <a href="{{ route('videos.create') }}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span>Add Video</span></a>
                </li>






            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
