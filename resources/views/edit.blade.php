@extends('layouts.app')
@include('layouts.sidebar')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
          <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Metadata</h4>

                                    <form method="POST" action="{{ route('videos.update', $video['id']) }}">
                                      @csrf
                                          <div class="form-group">
                                              <label for="name">Name</label>
                                              <input type="text" id="name" name="name" class="form-control" placeholder="Title" value="{{ $video['name'] }}">
                                          </div>

                                          <div class="form-group">
                                              <label for="desc">Description</label>
                                              <textarea name="desc" id="desc" class="form-control" rows="5" cols="80">{{ $video['desc'] }}</textarea>
                                          </div>

                                          <div class="form-group">
                                              <label for="tags">Tags</label>
                                              <input type="text" class="form-control" name="tags" id="tags" placeholder="Tags" value="{{ $video['tags'] }}">
                                          </div>
                                          <div class="form-group">
                                              <label for="category">Categories</label>
                                              <input type="text" name="category" class="form-control" id="category" placeholder="Categories" value="{{ $video['category'] }}">
                                          </div>
                                    @if ($errors->has('video'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('video') }}</strong>
                                        </span>
                                        <br>
                                    @endif
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    <a href="{{ route('videos.index') }}"><button type="button" class="btn btn-danger waves-effect waves-light">Close</button></a>
                                    </form>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
{{-- <script src="{{ asset('js/metismenu.min.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script> --}}
{{-- <script src="{{ asset('js/simplebar.min.js') }}"></script> --}}

<!-- third party js -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ asset('pages/datatables-demo.js') }}"></script>
@endsection
