@extends('layouts.app')
@include('layouts.sidebar')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
          <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">
                                      <h4>Content Manager ({{ count($videos) }})</h4>
                                      <div class="">
                                        <div class="form-group">
                                          <div class="row">
                                            <div class="col-sm-2">
                                              <label for="simpleinput">Search</label>
                                              <input type="text" id="title" class="form-control" placeholder="Enter your text">
                                            </div>
                                            <div class="col-sm-1">
                                              <label for="simpleinput">...........</label>
                                              <input type="button" onclick="search()" class="btn btn-primary" value="Search">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <table id="basic-datatable" class="table dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>Thumbnail</th>
                                                <th>Name</th>
                                                <th>ID</th>
                                                <th>Created On</th>
                                                <th>Duration</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>


                                        <tbody id="tBody">
                                          @foreach ($videos as $video)
                                            <tr id="{{ $video['id'] }}">
                                              <td><img src="{{ asset($video->thumb) }}" height="60"/></td>
                                              <td>{{ $video['name'] }}</td>
                                              <td>{{ $video['id'] }}</td>
                                              <td>{{ $video['created_at'] }}</td>
                                              <td>{{ $video['duration'] }}</td>
                                              <td> <a href="/videos/{{ $video['id'] }}/edit"/><i class="feather-edit-2"></i></a> / <a onclick="deletevideo({{ $video['id'] }})"/><i class="mdi mdi-delete"></i></a></td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                    </table>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
{{-- <script src="{{ asset('js/metismenu.min.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script> --}}
{{-- <script src="{{ asset('js/simplebar.min.js') }}"></script> --}}

<!-- third party js -->
        <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ asset('pages/datatables-demo.js') }}"></script>

        <script type="text/javascript">
          function deletevideo(e) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var videoId = e;
            $.ajax({
                    /* the route pointing to the post function */
                    url: '/videos/'+videoId,
                    type: 'DELETE',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, deleteId: videoId},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                        if (data.status == 'success') {
                          $('#'+e).remove()
                        }
                    }
                });
          }

          function search(e) {
            var title = $('#title').val();
            if (title != '') {
              var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
              var apiToken = {{ json_encode(Auth::user()->api_token) }}
              $.ajax({
                      /* the route pointing to the post function */
                      url: '/api/videos/search',
                      headers: {
                                  'Authorization':'Bearer ' + apiToken,
                                },
                      type: 'POST',
                      /* send the csrf-token and the input to the controller */
                      data: {title: title},
                      dataType: 'JSON',
                      /* remind that 'data' is the response of the AjaxController */
                      success: function (data) {
                        var oldDom = $('<tbody/>').append($('#tBody').clone()).html();
                        // console.log(oldDom);
                          if (data.result) {
                            $('#tBody').empty();
                            var newTBody = '';
                            $.each(data.result, function (index, value) {
                              $('#tBody').append('<tr id='+ value.id +'><td><img src=/'+ value.thumb +' height="60"/></td><td>'+value.name+'</td><td>'+value.id+'</td><td>'+value.created_at+'</td><td>'+value.duration+'</td><td><a href="/videos/'+value.id+'/edit" class=feather-edit-2/></a> / <i onclick=deletevideo('+value.id+') class="mdi mdi-delete"></i></td></tr>');
                            });

                          }
                      }
                  });
            }
          }
        </script>
@endsection
